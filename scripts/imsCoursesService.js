/* Darzustellende Daten: Klassengrössen der IMS Klassen
   1i, 2i und 3i von den Schuljahren 16/17 bis 20/21.
*/
const imsCourses = [
  {
    schoolYear: 'SJ 16/17',
    classSizes: [24, 29, 20],
  },
  {
    schoolYear: 'SJ 17/18',
    classSizes: [17, 25, 29],
  },
  {
    schoolYear: 'SJ 18/19',
    classSizes: [20, 18, 24],
  },
  {
    schoolYear: 'SJ 19/20',
    classSizes: [24, 19, 17],
  },
  {
    schoolYear: 'SJ 20/21',
    classSizes: [20, 23, 19],
  },
];

function getImsCourses() {
  return Promise.resolve(imsCourses);
}

export { getImsCourses };
